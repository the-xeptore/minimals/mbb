package devices

import (
	"io/ioutil"
	"net/http"
	"strings"
)

func fetchURL(url string) (string, error) {
	resp, err := http.Get(url)
	if nil != err {
		return "", err
	}
	defer resp.Body.Close()

	respBytes, err := ioutil.ReadAll(resp.Body)
	if nil != err {
		return "", err
	}

	builder := strings.Builder{}
	builder.Write(respBytes)

	return builder.String(), nil
}
