//+build irancell

package devices

import (
	"bytes"
	"fmt"
	"net/http"
	"regexp"
	"time"
)

var re, _ = regexp.Compile(`<token>(?P<token>\d+)<\/token>`)

func getToken() string {
	response, _ := fetchURL("http://192.168.8.1/api/webserver/token")
	return re.FindStringSubmatch(response)[1]
}

func switchData(state string) error {
	body := []byte(fmt.Sprintf("<?xml version=\"1.0\" encoding=\"UTF-8\"?><request><dataswitch>%s</dataswitch></request>", state))
	request, err := http.NewRequest("POST", "http://192.168.8.1/api/dialup/mobile-dataswitch", bytes.NewReader(body))
	if nil != err {
		return err
	}

	request.Header.Add("__requestverificationtoken", getToken())
	request.Header.Add("accept", "*/*")
	request.Header.Add("accept-language", "en-US,en;q=0.9")
	request.Header.Add("cache-control", "no-cache")
	request.Header.Add("content-type", "application/x-www-form-urlencoded; charset=UTF-8")
	request.Header.Add("x-requested-with", "XMLHttpRequest")

	client := http.Client{Timeout: 2 * time.Second}
	_, err = client.Do(request)
	if nil != err {
		return err
	}

	return nil
}

func ActivateConnection() error {
	return switchData("1")
}

func DeactivateConnection() error {
	return switchData("0")
}
