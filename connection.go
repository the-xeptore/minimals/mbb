package main

import (
	"net/http"
	"sync"
	"time"

	"gitlab.com/the-xeptore/minimals/mbb/devices"
)

func disconnect() error {
	log("Deactivating connection... 🤔")
	if err := devices.DeactivateConnection(); nil != err {
		log("Deactivating connection... 🙁")
		return err
	}
	time.Sleep(2 * time.Second)
	log("Deactivating connection... 🙂")

	return nil
}

func connect() error {
	log("Activating connection... 🤔")
	if err := devices.ActivateConnection(); nil != err {
		log("Activating connection... 🙁")
		return err
	}
	time.Sleep(5 * time.Second)
	log("Activating connection... 🙂")

	return nil
}

func reconnect() error {
	if err := disconnect(); nil != err {
		return err
	}
	if err := connect(); nil != err {
		return err
	}
	return nil
}

func canConnect(url string) bool {
	client := http.Client{Timeout: connectionTimeout}
	request, err := http.NewRequest("GET", url, nil)
	if nil != err {
		return false
	}
	resp, err := client.Do(request)
	if nil != err {
		return false
	}
	defer resp.Body.Close()

	return true
}

func checkConnectivity() bool {
	log("Cheking connectivity... 🤔")
	urls := []string{
		"https://avatars2.githubusercontent.com/u/29199390?s=60&v=4",
		"https://mediacdn.mediaad.org/1/19/image/1bf3465b-cc26-4403-83bc-7f85d57a0573.jpg",
		"https://p30download.com/template/images/search.png",
		"https://git.ir/static/img/git-logo-small.png",
	}

	wg := new(sync.WaitGroup)
	wg.Add(len(urls))

	results := make([]bool, len(urls))
	for i, url := range urls {
		i, url := i, url
		go func() {
			results[i] = canConnect(url)
			wg.Done()
		}()
	}
	wg.Wait()

	for i := 0; i < len(results); i++ {
		if results[i] {
			log("Cheking connectivity... 🙂")
			return true
		}
	}

	log("Cheking connectivity... 🙁")
	return false
}

func hasConnection(retries uint) bool {
	for i := 0; i < int(retries); i++ {
		if isConnected := checkConnectivity(); isConnected {
			return true
		}
	}

	return false
}
