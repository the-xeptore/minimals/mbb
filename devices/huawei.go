//+build huawei

package devices

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/base64"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"math/big"
	"net/http"
	"strconv"
	"strings"
	"time"

	"golang.org/x/net/html"
)

type pubkey struct {
	XMLName xml.Name `xml:"response"`
	N       *big.Int
	E       int
	Es      string `xml:"encpubkeye"`
	Ns      string `xml:"encpubkeyn"`
}

func getSession() (string, error) {
	resp, err := http.Get("http://192.168.8.1/html/home.html")
	if nil != err {
		return "", err
	}
	defer resp.Body.Close()

	return strings.Split(resp.Header.Get("set-cookie"), ";")[0], nil
}

func extractToken(response io.Reader) []string {
	// bs, _ := ioutil.ReadAll(response)
	// log.Fatalf("%s", string(bs))
	tokenizer := html.NewTokenizer(response)

	tokens := make([]string, 0, 2)

	for {
		tt := tokenizer.Next()
		if tt == html.EndTagToken {
			break
		}
		switch {
		case tt == html.SelfClosingTagToken:
			t := tokenizer.Token()
			if t.Data == "meta" {
				for _, attr := range t.Attr {
					if attr.Key == "name" && attr.Val == "csrf_token" {
						for _, attribs := range t.Attr {
							if attribs.Key == "content" {
								tokens = append(tokens, attribs.Val)
								break
							}
						}
					}
				}
			}
		}
	}

	return tokens
}

func getTokens(session string) ([]string, error) {
	req, err := http.NewRequest("GET", "http://192.168.8.1/html/home.html", nil)
	if nil != err {
		return nil, err
	}
	req.Header.Add("cookie", session)

	client := http.Client{}
	resp, err := client.Do(req)
	if nil != err {
		return nil, err
	}
	defer resp.Body.Close()

	return extractToken(resp.Body), nil
}

func switchData(auth *userAuth, state string) error {
	body := []byte(fmt.Sprintf("<?xml version=\"1.0\" encoding=\"UTF-8\"?><request><dataswitch>%s</dataswitch></request>", state))
	request, err := http.NewRequest("POST", "http://192.168.8.1/api/dialup/mobile-dataswitch", bytes.NewReader(body))
	if nil != err {
		return err
	}

	request.Header.Add("__RequestVerificationToken", auth.tokens[0])
	request.Header.Add("Cookie", auth.cookie)
	request.Header.Add("accept", "*/*")
	request.Header.Add("accept-language", "en-US,en;q=0.9")
	request.Header.Add("cache-control", "no-cache")
	request.Header.Add("content-type", "application/x-www-form-urlencoded; charset=UTF-8")
	request.Header.Add("x-requested-with", "XMLHttpRequest")

	client := http.Client{Timeout: 2 * time.Second}
	resp, err := client.Do(request)
	if nil != err {
		return err
	}
	defer resp.Body.Close()

	return nil
}

func getPubKey(session string) (*pubkey, error) {
	req, err := http.NewRequest("GET", "http://192.168.8.1/api/webserver/publickey", nil)
	if nil != err {
		return nil, err
	}
	req.Header.Add("cookie", session)

	client := http.Client{}
	resp, err := client.Do(req)
	if nil != err {
		return nil, err
	}
	defer resp.Body.Close()

	respBytes, err := ioutil.ReadAll(resp.Body)
	if nil != err {
		return nil, err
	}

	pk := new(pubkey)
	err = xml.Unmarshal(respBytes, pk)
	if nil != err {
		return nil, err
	}
	i, _ := strconv.ParseInt(pk.Es, 16, 32)
	pk.E = int(i)
	bn := new(big.Int)
	bn.SetString(pk.Ns, 16)
	pk.N = bn

	return pk, nil
}

type userAuth struct {
	tokens []string
	cookie string
}

func encrypt(text string, keys *pubkey) (string, error) {
	rng := rand.Reader

	builder := new(bytes.Buffer)
	builder.WriteString(text)
	encodedText := base64.StdEncoding.EncodeToString(builder.Bytes())

	pub := rsa.PublicKey{
		N: keys.N,
		E: keys.E,
	}
	encrypted := ""
	for i := 0; i < len(encodedText)/245+1; i++ {
		encData := encodedText[i*245 : min((i+1)*245, len(encodedText))]
		encodedTextBytes := new(bytes.Buffer)
		encodedTextBytes.WriteString(encData)
		enc, err := rsa.EncryptPKCS1v15(rng, &pub, encodedTextBytes.Bytes())
		if nil != err {
			return "", err
		}
		encrypted += fmt.Sprintf("%x", enc)
	}

	return encrypted, nil
}

func encodeSHA256(input []byte) string {
	return fmt.Sprintf("%x", sha256.Sum256(input))
}

func encodeBase64(input string) string {
	bt := bytes.Buffer{}
	bt.WriteString(input)
	return base64.StdEncoding.EncodeToString(bt.Bytes())
}

func login(username, password, token, session string) (*userAuth, error) {
	bt := bytes.Buffer{}
	bt.WriteString(password)
	tmp := encodeBase64(encodeSHA256(bt.Bytes()))
	bt.Reset()
	bt.WriteString(username)
	bt.WriteString(tmp)
	bt.WriteString(token)
	passwd := encodeBase64(encodeSHA256(bt.Bytes()))
	xmlBody := fmt.Sprintf("<?xml version=\"1.0\" encoding=\"UTF-8\"?><request><Username>%s</Username><Password>%s</Password><password_type>4</password_type></request>", username, passwd)

	keys, _ := getPubKey(session)

	body, _ := encrypt(xmlBody, keys)
	req, err := http.NewRequest("POST", "http://192.168.8.1/api/user/login", bytes.NewReader([]byte(body)))
	if nil != err {
		return nil, err
	}
	req.Header.Add("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0")
	req.Header.Add("Accept", "*/*")
	req.Header.Add("Accept-Language", "en-US,en;q=0.5")
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
	req.Header.Add("__RequestVerificationToken", token)
	req.Header.Add("encrypt_transmit", "encrypt_transmit")
	req.Header.Add("X-Requested-With", "XMLHttpRequest")
	req.Header.Add("cookie", session)
	req.Header.Add("referrer", "http://192.168.8.1/html/home.html")

	client := http.Client{}
	resp, err := client.Do(req)
	if nil != err {
		return nil, err
	}
	defer resp.Body.Close()

	return &userAuth{
		tokens: strings.Split(strings.TrimSpace(resp.Header.Get("__requestverificationtoken")), "#"),
		cookie: strings.TrimSpace(resp.Header.Get("set-cookie")),
	}, nil
}

func ActivateConnection() error {
	session, err := getSession()
	if nil != err {
		return err
	}
	tokens, err := getTokens(session)
	if nil != err {
		return err
	}
	auth, err := login("admin", "morteza6909", tokens[0], session)
	if nil != err {
		return err
	}
	return switchData(auth, "1")
}

func DeactivateConnection() error {
	session, err := getSession()
	if nil != err {
		return err
	}
	tokens, err := getTokens(session)
	if nil != err {
		return err
	}
	auth, err := login("admin", "morteza6909", tokens[0], session)
	if nil != err {
		return err
	}
	return switchData(auth, "0")
}
